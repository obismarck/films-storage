'use strict';

const App = require('./app');

const start = async () => {
  const app = new App({ logger: { prettyPrint: true } });
  const server = await app.build();

  server.listen(process.env.PORT, process.env.HOST, err => {
    if (err)
      process.exit(1);
  });
};

start();
