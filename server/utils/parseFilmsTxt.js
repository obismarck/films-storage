'use strict';

const createError = require('fastify-error');

const InvalidArgumentError = createError('INVALID_ARGUMENT', '%s', 400);

const parseTitle = titleStr => {
  if (titleStr.startsWith('Title: ')) {
    const title = titleStr.replace('Title: ', '').trim();

    // Check whether title is not empty
    if (title)
      return title;
  }

  throw new InvalidArgumentError(`Invalid "Title" value: ${titleStr}`);
};

const parseReleaseYear = releaseYearStr => {
  if (releaseYearStr.startsWith('Release Year: ')) {
    const year = Number.parseInt(
      releaseYearStr.replace('Release Year: ', '').trim()
    );

    // Check whether year exists and is included into bounds
    if (year >= 1850 && year <= 2021)
      return year;
  }

  throw new InvalidArgumentError(
    `Invalid "Release Year" value: ${releaseYearStr}`
  );
};

const parseFormat = formatStr => {
  // All available formats
  const formats = /^(VHS|DVD|Blu-Ray)$/;

  if (formatStr.startsWith('Format: ')) {
    const format = formatStr.replace('Format: ', '').trim();

    // Check whether format is available
    if (formats.test(format)) return format;
  }

  throw new InvalidArgumentError(`Invalid "Format" value: ${formatStr}`);
};

const parseStars = starsStr => {
  // First and last names reg exp
  const alpha = /^[A-Z]+ [A-Z ]+$/i;

  if (starsStr.startsWith('Stars: ')) {
    const stars = starsStr.replace('Stars: ', '').split(', ');

    // Check the star's first and last names
    if (stars.every(s => alpha.test(s))) {
      // Conversion to set returns only unique values
      const starsSet = new Set(stars.map(s => s.trim()));
      return Array.from(starsSet);
    }
  }

  throw new InvalidArgumentError(`Invalid "Stars" values: ${starsStr}`);
};

const parseFilmsTxt = text => {
  const filmStrs = text.trim().split('\n\n');

  // Invalid file format or empty file
  if (filmStrs.length === 0) throw new InvalidArgumentError('Invalid file');

  const films = filmStrs.map(filmStr => {
    const fields = filmStr.split('\n');

    // Invalid fields count
    if (fields.length !== 4)
      throw new InvalidArgumentError('Invalid fields format');

    const [titleStr, releaseYearStr, formatStr, starsStr] = fields;

    // Parse fields
    const title = parseTitle(titleStr);
    const releaseYear = parseReleaseYear(releaseYearStr);
    const format = parseFormat(formatStr);
    const stars = parseStars(starsStr);

    return { title, releaseYear, format, stars };
  });

  return films;
};

module.exports = parseFilmsTxt;
