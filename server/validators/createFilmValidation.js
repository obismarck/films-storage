'use strict';

const createError = require('fastify-error');
const InvalidArgumentError = createError('INVALID_ARGUMENT', '%s', 400);

const titleValidator = async (request, reply) => {
  const { title } = request.body;

  if (!title)
    throw new InvalidArgumentError('title field was not provided');
}

const releaseYearValidator = async (request, reply) => {
  const { releaseYear } = request.body;

  if (!releaseYear)
    throw new InvalidArgumentError('"releaseYear" field was not provided');

  if (!Number.isInteger(releaseYear))
    throw new InvalidArgumentError('releaseYear is not a number');

  if (releaseYear < 1850 || releaseYear > 2021)
    throw new InvalidArgumentError('releaseYear must belong to 1850 - 2021 bounds');
};

const formatValidator = async (request, reply) => {
  const { format } = request.body;

  if (!format)
    throw new InvalidArgumentError('"format" field was not provided');

  if (!/^(VHS|DVD|Blu-Ray)$/.test(format))
    throw new InvalidArgumentError('Film format is not supported');
};

const starsValidator = async (request, reply) => {
  const { stars } = request.body;

  if (!stars)
    throw new InvalidArgumentError('"stars" field was not provided');

  if (!stars.every(s => /^[A-Z]+ [A-Z ]+$/i.test(s)))
    throw new InvalidArgumentError(
      'Stars are not in "FirstName LastName" format'
    );
};

module.exports = {
  titleValidator,
  releaseYearValidator,
  formatValidator,
  starsValidator
};

