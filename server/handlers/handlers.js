'use strict';

const parseFilmsTxt = require('../utils/parseFilmsTxt');

const getFilms = api => async (request, reply) => {
  const films = await api.database.getFilms();
  reply.status(200).send(films);
};

const createFilm = api => async (request, reply) => {
  const { title, releaseYear, format, stars } = request.body;

  const starsSet = new Set(stars);
  const uniqueStars = Array.from(starsSet);

  await api.database.createFilms([{ title, releaseYear, format, stars: uniqueStars }]);
  reply.status(201).send({ message: 'Film was successfully created' });
};

const deleteFilm = api => async (request, reply) => {
  const { filmId } = request.params;
  const res = await api.database.deleteFilm(filmId);

  if (!res)
    return reply.status(404).send({ message: 'Unable delete non-existing film' });

  reply.send({ message: 'Film was successfully deleted' });
};

const importFilms = api => async (request, reply) => {
  const fileStream = await request.file();

  // Test the filename extension and mimetype
  const { filename, mimetype } = fileStream;
  if (!/^.*\.txt$/.test(filename) || mimetype !== 'text/plain')
    return reply.status(400).send({ message: 'Invalid file format. Must be a .txt' });

  const buffer = await fileStream.toBuffer();
  const text = buffer.toString();

  const importedCount = await api.database.createFilms(parseFilmsTxt(text));
  reply.status(201).send({ message: `${importedCount} films were successfully imported` });
};

module.exports = {
  getFilms,
  createFilm,
  deleteFilm,
  importFilms
};
