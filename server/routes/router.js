'use strict';

const {
  getFilms,
  createFilm,
  deleteFilm,
  importFilms
} = require('../handlers/handlers');

const {
  titleValidator,
  formatValidator,
  releaseYearValidator,
  starsValidator
} = require('../validators/createFilmValidation');

const defineRoutes = (fastify, api) => {
  fastify.get('/api/films', {
    handler: getFilms(api)
  });

  fastify.post('/api/films/import', {
    handler: importFilms(api)
  });

  fastify.post('/api/films', {
    handler: createFilm(api),
    preValidation: [titleValidator, releaseYearValidator, formatValidator, starsValidator]
  });

  fastify.delete('/api/films/:filmId', {
    handler: deleteFilm(api)
  });
};

module.exports = defineRoutes;
