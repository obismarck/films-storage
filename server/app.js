'use strict';

const path = require('path');
const dotenv = require('dotenv');
const fastify = require('fastify');
const mongoose = require('mongoose');
const cors = require('fastify-cors');
const multipart = require('fastify-multipart');
const stat = require('fastify-static');

const defineRoutes = require('./routes/router');
const MongoDatabase = require('./database/MongoDatabase');

class App {
  constructor(options = {}) {
    this.app = fastify(options);
  }

  async build() {
    try {
      // Load project configuration
      dotenv.config({ path: '.env' });

      // Connect to MongoDB
      await mongoose.connect(process.env.MONGOURI, { useNewUrlParser: true, useUnifiedTopology: true });

      // Set up CORS
      this.app.register(cors, {
        origin: (origin, cb) => cb(null, true)
      });

      // Set up multipart data type handler
      this.app.register(multipart);

      // Set React build as static folder
      this.app.register(stat, {
        root: path.resolve(__dirname, '../client/build')
      });

      // Form api object
      const api = {
        database: new MongoDatabase()
      };

      // Create routes
      defineRoutes(this.app, api);

      // Return built fastify instance
      return this.app;
    } catch(e) {
      console.log(e.message);
      process.exit(1);
    }
  }
}

module.exports = App;
