'use strict';

const mongoose = require('mongoose');
const { Schema, model } = mongoose;

const filmSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  stars: {
    type: [String],
    required: true
  },
  releaseYear: {
    type: Number,
    required: true
  },
  format: {
    type: String,
    required: true
  }
});

module.exports = model('Film', filmSchema);
