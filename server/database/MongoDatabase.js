'use strict';

const createError = require('fastify-error');
const Film = require('./models/film');

const DatabaseError = createError('INTERNAL_SERVER_ERROR', 'Server error occured', 500);

class MongoDatabase {
  async getFilms() {
    try {
      const films = await Film.find();
      return films;
    } catch(e) {
      throw new DatabaseError();
    }
  }

  async createFilms(films) {
    try {
      const res = await Film.insertMany(films);
      return res.length;
    } catch(e) {
      throw new DatabaseError();
    }
  }

  async deleteFilm(filmId) {
    try {
     const res = await Film.findOneAndDelete({ _id: filmId });
     return res;
    } catch(e) {
      throw new DatabaseError();
    }
  }
}

module.exports = MongoDatabase;
