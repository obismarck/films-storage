import React from 'react';

import './Alert.scss';

const Alert = ({ msg }) => {
  return (
    <div className='alert'>
      { msg }
    </div>
  );
};

export default Alert;
