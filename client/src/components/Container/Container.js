import React from 'react';

import FilmList from './FilmList/FilmList';
import Menu from './Menu/Menu';

import './Container.scss';

const Container = () => {
  return <div className="container">
    <Menu />
    <FilmList />
  </div>;
};

export default Container;
