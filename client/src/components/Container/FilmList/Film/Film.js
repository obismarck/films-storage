import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { ExpandMore, RemoveCircle } from '@material-ui/icons';
import { removeFilm } from '../../../../redux/actions';

import './Film.scss';

const Film = ({ id, title, releaseYear, format, stars }) => {
  const dispatch = useDispatch();
  const [hidden, setHidden] = useState(true);

  const onInfoClick = () => setHidden(!hidden);
  const onDeleteClick = () => {
    const res = window.confirm(`Are you sure you want to delete film "${title}"?`);
    if (res)
      dispatch(removeFilm(id));
  }

  return (
    <div className='film'>
      <div className='film-header'>
        <span className='film-title'>{title}</span>
        <div className='buttons'>
          <button onClick={onInfoClick}><ExpandMore /></button>
          <button onClick={onDeleteClick}><RemoveCircle style={{ fill: 'red' }}/></button>
        </div>
      </div>
      <div hidden={hidden}>
        <div className='film-body'>
          <span className='film-field'>Id: {id}</span>
          <span className='film-field'>Release year: {releaseYear}</span>
          <span className='film-field'>Format: {format}</span>
          <span className='film-field'>Stars: {stars.join(', ')}</span>
        </div>
      </div>
    </div>
  )
};

export default Film;
