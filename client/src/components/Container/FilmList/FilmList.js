import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { loadFilms, changeSort } from '../../../redux/actions';
import { SortByAlpha } from '@material-ui/icons';

import Film from './Film/Film';

import './FilmList.scss';

const FilmList = () => {
  const dispatch = useDispatch();
  useEffect(() => dispatch(loadFilms()), []);

  let { isAscSort, filteredFilms } = useSelector(state => state);
  if (isAscSort)
    filteredFilms.sort((f1, f2) => f1.title >= f2.title ? 1 : -1)
  else
    filteredFilms.sort((f1, f2) => f1.title >= f2.title ? -1 : 1)

  const onClick = () => dispatch(changeSort());

  return (
    <div>
      <span className='title'>Films list</span>
      <div className='film-list-sort'>
        <span>Sort: {isAscSort ? 'Ascending' : 'Descending'}</span>
        <button onClick={onClick}><SortByAlpha /></button>
      </div>
      <div className="film-list">
        {filteredFilms.map(film => <Film key={film._id} id={film._id} title={film.title} releaseYear={film.releaseYear} format={film.format} stars={film.stars}/>)}
      </div>
    </div>
  );
};

export default FilmList;
