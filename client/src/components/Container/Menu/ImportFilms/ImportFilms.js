import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { importFilms, setAlert } from '../../../../redux/actions';

import './ImportFilms.scss';

const ImportFilms = () => {
  const dispatch = useDispatch();
  const [file, setFile] = useState(null);

  const onChange = e => setFile(e.target.files[0]);
  const onSubmit = e => {
    e.preventDefault();

    if (!file)
      return dispatch(setAlert('No file was selected'));

    document.getElementById('import-form').reset();

    if (!/^.*\.txt$/.test(file.name) || file.type !== 'text/plain')
      return dispatch(setAlert('File type is wrong. Must be .txt'));

    const formData = new FormData();
    formData.append('films', file);
    dispatch(importFilms(formData));

    setFile(null);
    document.getElementById('import-form').reset();
  }

  return (
    <div className='import-films'>
      <span className='title'>Import films</span>
      <form id='import-form' onSubmit={onSubmit}>
        <input type='file' onChange={onChange}/>
        <button type='submit'>Send</button>
      </form>
    </div>
  )
};

export default ImportFilms;
