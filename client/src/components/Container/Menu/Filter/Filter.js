import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { filterFilms } from '../../../../redux/actions';

import './Filter.scss';

const Filter = () => {
  const dispatch = useDispatch();
  const [filterFields, setFilterFields] = useState({
    title: '',
    star: ''
  });

  const { title, star } = filterFields;

  const onChange = e => {
    const update = {
      ...filterFields,
      [e.target.name]: e.target.value
    };

    setFilterFields(update);
    dispatch(filterFilms(update));
  }

  return (
    <div className='filter'>
      <span className='title'>Filter</span>
      <label>Title:</label>
      <input type='text' name='title' value={title} onChange={onChange}/>
      <label>Star:</label>
      <input type='text' name='star' value={star} onChange={onChange}/>
    </div>
  )
};

export default Filter;
