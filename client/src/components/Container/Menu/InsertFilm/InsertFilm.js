import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { createFilm, setAlert } from '../../../../redux/actions';

import './InsertFilm.scss';

const InsertFilm = () => {
  const dispatch = useDispatch();
  const initialFilm = {
    title: '',
    releaseYear: '',
    format: '',
    stars: [],
    inputStar: '',
    selectedStar: ''
  };
  const [film, setFilm] = useState(initialFilm);

  const { title, releaseYear, format, stars, inputStar, selectedStar } = film;

  const onChange = e => setFilm({ ...film, [e.target.name]: e.target.value });
  const onAddClick = e => {
    e.preventDefault();

    if (!inputStar)
      return;

    if (stars.includes(inputStar)) {
      dispatch(setAlert('Star is already in the list'));
      return setFilm({ ...film, inputStar: '' });
    }

    setFilm({ ...film, stars: [...stars, inputStar], inputStar: '' });
  };

  const onRemoveClick = e => {
    e.preventDefault();

    setFilm({ ...film, stars: stars.filter(s => s !== selectedStar), selectedStar: '' });
  }

  const onSubmit = e => {
    e.preventDefault();

    dispatch(createFilm({ title, releaseYear: Number.parseInt(releaseYear), format, stars }));
    setFilm(initialFilm);
  }

  return (
    <div className='insert-film'>
      <span className='title'>Insert a film</span>
      <form className='form-insert' onSubmit={onSubmit}>
        <div className='form-group'>
          <label>Title:</label>
          <input type='text' name='title' value={title} onChange={onChange}/>
        </div>
        <div className='form-group'>
          <label>Release Year:</label>
          <input type='number' name='releaseYear' value={releaseYear} onChange={onChange}/>
        </div>
        <div className='form-group'>
          <label>Format: </label>
          <select name='format' onChange={onChange}>
            <option value=''>Choose the format</option>
            <option value='VHS'>VHS</option>
            <option value='DVD'>DVD</option>
            <option value='Blu-Ray'>Blu-Ray</option>
          </select>
        </div>
        <div className='form-group'>
          <label>Stars: </label>
          <input type='text' name='inputStar' value={inputStar} onChange={onChange}/>
          <div className='star-buttons'>
            <button onClick={onAddClick}>Add</button>
            <button onClick={onRemoveClick}>Remove</button>
          </div>
          <select size='4' name='selectedStar' onChange={onChange}>
            {stars.map(star => <option>{star}</option>)}
          </select>
        </div>
        <button type='submit'>Insert</button>
      </form>
    </div>
  )
};

export default InsertFilm;
