import React from 'react';

import Filter from './Filter/Filter';
import InsertFilm from './InsertFilm/InsertFilm';
import ImportFilms from './ImportFilms/ImportFilms';

const Menu = () => {
  return (
    <div className='menu'>
      <Filter />
      <InsertFilm />
      <ImportFilms />
    </div>
  )
};

export default Menu;
