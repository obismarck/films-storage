import axios from 'axios';

import {CHANGE_SORT, FILTER_FILMS, LOAD_FILMS, REMOVE_ALERT, SET_ALERT} from './types';

export const setAlert = msg => {
  return async dispatch => {
    dispatch({ type: SET_ALERT, payload: { msg } });
    setTimeout(() => dispatch({ type: REMOVE_ALERT }), 3500);
  }
};

export const loadFilms = () => {
  return async dispatch => {
    try {
      const { data } = await axios.get('/api/films');
      dispatch({ type: LOAD_FILMS, payload: data });
    } catch(e) {
      const { message } = e.response.data;
      dispatch(setAlert(message));
    }
  };
};

export const createFilm = film => {
  return async dispatch => {
    try {
      await axios.post(`/api/films`, film);
      dispatch(loadFilms());
    } catch(e) {
      const { message } = e.response.data;
      dispatch(setAlert(message));
    }
  };
};

export const importFilms = formData => {
  return async dispatch => {
    try {
      await axios.post(`/api/films/import`, formData, { 'Content-Type': 'multipart/form-data' });
      dispatch(loadFilms());
    } catch(e) {
      const { message } = e.response.data;
      dispatch(setAlert(message));
    }
  };
};

export const removeFilm = filmId => {
  return async dispatch => {
    try {
      await axios.delete(`/api/films/${filmId}`);
      dispatch(loadFilms());
    } catch(e) {
      const { message } = e.response.data;
      dispatch(setAlert(message));
    }
  };
};

export const filterFilms = fields => {
  return {
    type: FILTER_FILMS,
    payload: fields
  };
};

export const changeSort = () => {
  return { type: CHANGE_SORT };
};
