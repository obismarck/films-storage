import { CHANGE_SORT, FILTER_FILMS, LOAD_FILMS, REMOVE_ALERT, SET_ALERT } from './types';

const initialState = {
  films: [],
  filteredFilms: [],
  isAscSort: true,
  alert: null
};

const filterFunction = (films, sortFields) => {
  const { title, star } = sortFields;

  return films.filter(film => {
    const partOfTitle = film.title.toLowerCase().includes(title.toLowerCase());
    const partOfStar = film.stars.some(s => s.toLowerCase().includes(star.toLowerCase()));

    return partOfTitle && partOfStar;
  });
};

const rootReducer = (state = initialState, action) => {
  switch(action.type) {
    case LOAD_FILMS:
      return { ...state, films: action.payload, filteredFilms: action.payload }
    case FILTER_FILMS:
      return {
        ...state,
        filteredFilms: filterFunction([...state.films], action.payload)
      }
    case CHANGE_SORT:
      return {
        ...state,
        isAscSort: !state.isAscSort
      }
    case SET_ALERT:
      return {
        ...state, alert: action.payload
      }
    case REMOVE_ALERT:
      return {
        ...state, alert: null
      }
    default:
      return state;
  }
};

export default rootReducer;
