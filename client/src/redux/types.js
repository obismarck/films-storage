export const LOAD_FILMS = 'LOAD_FILMS';
export const FILTER_FILMS = 'FILTER_FILMS';
export const CHANGE_SORT = 'CHANGE_SORT';

export const SET_ALERT = 'SET_ALERT';
export const REMOVE_ALERT = 'REMOVE_ALERT';
