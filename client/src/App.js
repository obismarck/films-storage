import React from 'react';
import { useSelector } from 'react-redux';

import Container from './components/Container/Container';
import Alert from './components/layout/Alert/Alert';
import './App.scss';

function App() {
  const alert = useSelector(state => state.alert);

  return (
    <div className="App">
      {alert && <Alert msg={alert.msg}/>}
      <Container />
    </div>
  );
}

export default App;
