# films-storage

## Install

* Configure your `.env` file in the project root folder
    * `HOST` *Default 127.0.0.1*
    * `PORT` *Default 8080*
    * `MONGODBURI` - URI of your database 
  
* Run `npm build` in the project root folder
* Run `npm start` to launch the server

## Description
The **films-storage** project offers a films storage and manipulation on it. It allows to store a list of your films, add a new film, remove a film or import films from a .txt file.

### Technologies ###
* `Node.js`
* `Fastify`
* `React`
* `Redux`
* `MongoDB`

### API ###
### `GET:/api/films` ###
Returns array of all the films in the storage.


### `POST:/api/films` ###
Creates a new film document from given JSON `body` value.
Body have to contain fields:
* `title` - Film title: *string*
* `releaseYear` - Film release year: *number*
* `format` - Film format: *string*. *Must be "DVD", "VHS" or "Blu-Ray" value*
* `stars` - Film actors: *Array< string >*. *Must be in "FirstName LastName" format*

### `DELETE:/api/films/filmId` ###
Deletes a film by id. If id is missing, returns an error.

### `POST:/api/films/import` ###
Imports films from a `.txt` file. \
File example: \
`Title: Once Upon A Time ... in Hollywood` *String value* \
`Release Year: 2019` *Number value* \
`Format: Blu-Ray` *Restricted to "DVD", "VHS", "Blu-Ray"* \
`Stars: Brad Pitt, Leonardo DiCaprio, Margot Robbie` *Actors have to be in "FirstName LastName" format, divided by `,`* \
Every film is divided by `\n\n`;

### Design ###
* `app.js` - Implements `App` class, which declares routes, registers plugins and builds server instance 
* `server.js` - Root file, which gets built server from `app.js` and launches it
* `./database` - Contains `mongoose` schemas and DB interaction class with necessary queries
* `./handlers` - Includes controllers, which provide the functionality
* `./routes` - Declares routes, set handlers and validation hooks
* `./utils` - Contains file parsing utility
* `./validators` - Input data validators





